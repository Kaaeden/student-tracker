package com.rave.studenttracker.view.student

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CardElevation
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.rave.studenttracker.model.entity.Student

@Composable
fun StudentListScreen(students: List<Student>) {
    LazyColumn {
        items(
            items = students,
            key = { student: Student -> student.id }
        ) { student: Student ->
            StudentCard(student = student)
        }
    }
}

@Composable
fun StudentCard(student: Student) {
    Card(modifier = Modifier
        .fillMaxSize()
        .padding(15.dp), elevation = CardDefaults.cardElevation(10.dp)) {
        Column() {
            Box {
                Column() {


                    AsyncImage(
                        model = student.avatar,
                        contentDescription = "Student-Avatar",
                        modifier = Modifier.size(60.dp)
                    )
                    Row() {
                        Text(text = student.firstName)
                        Text(text = student.lastName)
                    }
                }
            }
            Text(text = "University: ${student.university}")
            Text(text = "Email: ${student.email}")
            }
    }

}
